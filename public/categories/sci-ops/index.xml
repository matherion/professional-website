<?xml version="1.0" encoding="utf-8" standalone="yes" ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
  <channel>
    <title>Sci-Ops | Gjalt-Jorn Peters&#39; website</title>
    <link>https://behaviorchange.eu/categories/sci-ops/</link>
      <atom:link href="https://behaviorchange.eu/categories/sci-ops/index.xml" rel="self" type="application/rss+xml" />
    <description>Sci-Ops</description>
    <generator>Source Themes Academic (https://sourcethemes.com/academic/)</generator><language>en-us</language><lastBuildDate>Wed, 13 Nov 2019 00:00:00 +0000</lastBuildDate>
    <image>
      <url>https://behaviorchange.eu/img/logo.png</url>
      <title>Sci-Ops</title>
      <link>https://behaviorchange.eu/categories/sci-ops/</link>
    </image>
    
    <item>
      <title>Collaborating on reproducible manuscripts for dummies: an introduction using git, R Markdown and Zotero</title>
      <link>https://behaviorchange.eu/2019/11/collaborating-on-reproducible-manuscripts-for-dummies-an-introduction-using-git-r-markdown-and-zotero/</link>
      <pubDate>Wed, 13 Nov 2019 00:00:00 +0000</pubDate>
      <guid>https://behaviorchange.eu/2019/11/collaborating-on-reproducible-manuscripts-for-dummies-an-introduction-using-git-r-markdown-and-zotero/</guid>
      <description>


&lt;p&gt;This document is an introduction to a workflow that facilitates collaborating with other researchers if your aim is to apply Open Science principles.&lt;/p&gt;
&lt;div id=&#34;background&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;Background&lt;/h1&gt;
&lt;p&gt;This workflow is consistent with the following Open Science principles:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&lt;p&gt;&lt;strong&gt;Being as transparent as possible.&lt;/strong&gt; In scientific publications of empirical endeavours, it is important that people can inspect how the results derive from the collected data. One way to achieve this is to include deparate analysis scripts and let the readers (and the future ‘you’) sort things out for themselves. However, this still leaves a lot of room for interpretation. Ideally, every reported result can be traced back unequivocally to analyses and data. Reproducible manuscripts enable you to achieve this with relatively little effort. This will serve your future self, your colleagues, and the scientific community.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;strong&gt;Building an Open infrastructure for our science.&lt;/strong&gt; One of the Open Science foundations is inclusiveness. To build an inclusive science requires a number of things. One is writing tutorials such as this one, so that learning the required competences is in everybody’s reach. Another is to use Free/Libre Open Source Software (FLOSS) and open standards as much as possible, and use proprietary software as little as possible. This is important because one of the many divides between scientists world-wise is financial. Choosing to use proprietary software therefore excludes a part of the scientific community, either now or potentially in the future (in those cases where limited free versions of proprietary software exist).&lt;/p&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;This workflow is designed to be relatively easy to master; although some quite advanced tools are used (e.g. R Markdown and git), you only use very limited functionality from each.&lt;/p&gt;
&lt;/div&gt;
&lt;div id=&#34;the-tools&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;The tools&lt;/h1&gt;
&lt;p&gt;This workflow uses the following tools:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&lt;p&gt;&lt;strong&gt;Markdown&lt;/strong&gt; for writing the manuscript text. Why Markdown? Because it’s much easier than HTML, and therefore, much much much easier than LaTeX. Also, HTML can be included, if desired, as can LaTeX Math expressions, in R Markdown. In this sense Markdown represents the sweet spot between being accessible to new users and powerful enough to fit most use cases.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;strong&gt;R&lt;/strong&gt; for the analyses. Why R? Because R is both very powerful and FLOSS. Furthermore, R has a lot of freely available handbooks and tutorials geared towards research in psychology and statistics. Finally, R has R Markdown, which allows embedding chunks of R code in Markdown documents.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;strong&gt;R Markdown&lt;/strong&gt; to integrate the analyses with the manuscript text, pretty much for the above reasons.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;strong&gt;RStudio&lt;/strong&gt; as an environment to do the writing in. RStudio allows rendering R Markdown documents with the press of a button, and integrates with git.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;strong&gt;git&lt;/strong&gt; for version/revision control and syncing. Git is FLOSS and extremely powerful, but also allows relative novices to play around with with relatively few challenges.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;strong&gt;GitLab&lt;/strong&gt; as online headquarters and to manage the project. GitLab is a FLOSS git management system with a number of nice extras such as issue management and so-called continuous integration.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;&lt;strong&gt;Zotero&lt;/strong&gt; for reference management. Zotero is a FLOSS reference manager that has an online API, which is exactly what we need to automagically have updated references in our manuscript.&lt;/p&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;/div&gt;
&lt;div id=&#34;the-installation&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;The installation&lt;/h1&gt;
&lt;p&gt;Of course, before you can collaborate on or start a project with this workflow, you need those tools installed and configured properly. This will only need to happen once on every PC, of course - once everything’s installed and configured, you can use it in all projects.&lt;/p&gt;
&lt;p&gt;You need the following:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;&lt;p&gt;An account at a GitLab installation (GitLab over GitHub because GitLab is FLOSS and GitHub isn’t). Either use your institution’s GitLab installation or create a free account at &lt;a href=&#34;https://gitlab.com&#34; class=&#34;uri&#34;&gt;https://gitlab.com&lt;/a&gt;.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;An account at a Zotero installation. Again, either use your institution’s installation or create a free account at &lt;a href=&#34;https://zotero.org&#34; class=&#34;uri&#34;&gt;https://zotero.org&lt;/a&gt;.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;The following software:&lt;/p&gt;
&lt;ul&gt;
&lt;li&gt;R (&lt;a href=&#34;https://cran.r-project.org/&#34; class=&#34;uri&#34;&gt;https://cran.r-project.org/&lt;/a&gt;)&lt;/li&gt;
&lt;li&gt;The RStudio desktop client (&lt;a href=&#34;https://rstudio.com/products/rstudio/#rstudio-desktop&#34; class=&#34;uri&#34;&gt;https://rstudio.com/products/rstudio/#rstudio-desktop&lt;/a&gt;; or access to an RStudio server installation)&lt;/li&gt;
&lt;li&gt;git (&lt;a href=&#34;https://git-scm.com/&#34; class=&#34;uri&#34;&gt;https://git-scm.com/&lt;/a&gt;)&lt;/li&gt;
&lt;li&gt;The Zotero desktop client (&lt;a href=&#34;https://www.zotero.org/download/&#34; class=&#34;uri&#34;&gt;https://www.zotero.org/download/&lt;/a&gt;)&lt;/li&gt;
&lt;/ul&gt;&lt;/li&gt;
&lt;/ul&gt;
&lt;p&gt;Once you installed R, git, and RStudio, RStudio should automatically find both R and git. You can check this by clicking “File” in RStudio, then clicking “New Project”, and then you should be able to create a new project using Version Control. Click it, and then click the Git option. If this works, RStudio found your git. No luck? Reboot your PC. Still no? See &lt;a href=&#34;https://happygitwithr.com/rstudio-see-git.html&#34; class=&#34;uri&#34;&gt;https://happygitwithr.com/rstudio-see-git.html&lt;/a&gt;.&lt;/p&gt;
&lt;/div&gt;
&lt;div id=&#34;the-project-set-up&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;The project set-up&lt;/h1&gt;
&lt;p&gt;Once you have the necessary software and accounts set up, you only have to do a few things to start a new project.&lt;/p&gt;
&lt;ol style=&#34;list-style-type: decimal&#34;&gt;
&lt;li&gt;&lt;p&gt;Log in to GitLab. Create a new project. Try to set a name that is as much as possible self-explanatory (to others, not to you) but not too long, and set the project to Public (it’s &lt;em&gt;Open&lt;/em&gt; Science, after all). Once created, you’re taken to the project overview: click the ‘Clone’ button and copy the “https” link.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;Open RStudio. In the File menu, select New Project, and create a new Version Control project, specifically, a Git project. Copy-paste the “Clone URL” that you just copied from GitLab. Pick a good place for the project (mine are all in a “Research” directory) and create it.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;RStudio now created an (as yet mostly empty) directory for you. In that directory, create subdirectories to structure your project.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;Log in to Zotero. Create a new public group (but let only admins add and edit entries). I recommend using the same name you used for the Git project.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;In RStudio, create a new file of the R Script type. Go to … and copy-paste the contents.&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;In RStudio, create a new file of the R Markdown type. This will be where you type your manuscript. As a convention, I recommend giving it a filename that’s equal to the git repo name (i.e. you project’s name with only lowercase letters and dashes instead of spaces).&lt;/p&gt;&lt;/li&gt;
&lt;li&gt;&lt;p&gt;In the project’s root, create a new file called “.gitlab-ci.yml”, and copy-paste the contents from …&lt;/p&gt;&lt;/li&gt;
&lt;/ol&gt;
&lt;p&gt;Now, you have the project set up so that most of the tasks are automated. This means&lt;/p&gt;
&lt;/div&gt;
&lt;div id=&#34;working-in-your-project&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;Working in your project&lt;/h1&gt;
&lt;p&gt;Once everything’s set up, you can start working in your project.&lt;/p&gt;
&lt;ol style=&#34;list-style-type: decimal&#34;&gt;
&lt;li&gt;&lt;/li&gt;
&lt;/ol&gt;
&lt;/div&gt;
</description>
    </item>
    
    <item>
      <title>A poor person&#39;s guide to Open Sciencing GDPR compliant data management</title>
      <link>https://behaviorchange.eu/2019/09/a-poor-person-s-guide-to-open-sciencing-gdpr-compliant-data-management/</link>
      <pubDate>Sat, 14 Sep 2019 00:00:00 +0000</pubDate>
      <guid>https://behaviorchange.eu/2019/09/a-poor-person-s-guide-to-open-sciencing-gdpr-compliant-data-management/</guid>
      <description>


&lt;p&gt;In this brief post I’ll outline a simple data management strategy that is consistent with both GDPR and Open Science principles. For readers unfamiliar with these: the EU’s General Data Protection Regulation (GDPR) somewhat sternly encourages treating personal data decently, and Open Science principles promote sharing data as much as possible.&lt;/p&gt;
&lt;div id=&#34;when-are-data-personal&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;When are data personal?&lt;/h1&gt;
&lt;p&gt;We deal with this in more detail in &lt;a href=&#34;https://doi.org/10.1080/08870446.2019.1606222&#34;&gt;Crutzen, Peters &amp;amp; Mondschein (2019)&lt;/a&gt;, but basically, personal data are data about a person. So: I am 1 meter and 75 centimeters tall. Because you know that is about me, it is personal data. However, if the average tallness of people in my city is 1.75 meters, that number is not personal data. Instead, it is a fact about the world. Of course, if I would be the only person living in my city, it would be excessively easy to figure out my tallness, which would again render the city average personal data.&lt;/p&gt;
&lt;p&gt;The crux, therefore, is identifiability. This means that sampling frame is very important. If a sample is truly aselect, e.g. sampled from millions of people, single variables such as age in years or religion are not personal data. However, if enough such columns are combined, identification becomes possible which then renders all columns personal data - after all, they are then all about persons.&lt;/p&gt;
&lt;/div&gt;
&lt;div id=&#34;what-do-you-have-to-do-if-you-handle-person-data&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;What do you have to do if you handle person data?&lt;/h1&gt;
&lt;p&gt;Personal data are always owned by the respective persons. Others can only process the data for them. The GDPR holds that this processing by definition always has a temporary nature, and must abide by some rules. These include the responsibility to properly log all processing that is done; make it easy for people to view, change or remove their personal data; maintain a list of everybody who has access to the data; and obviously, prevent data leaks.&lt;/p&gt;
&lt;/div&gt;
&lt;div id=&#34;open-sciencing-publishing-all-data&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;Open Sciencing: publishing all data&lt;/h1&gt;
&lt;p&gt;Open Science, on the other hand, encourages researchers to make all data public as soon as possible. There are very many good reasons for this, but of course it’s hard to combine with the GDPR requirements. Or is it? The solution lies in anonymization. The GDPR only deals with personal data. Once data are no longer identifiable, they can be safely shared.&lt;/p&gt;
&lt;p&gt;So, the question that plagues the integreous psychological scientist is: how do I satisfy both the GDPR and Open Science principles simultaneously?&lt;/p&gt;
&lt;/div&gt;
&lt;div id=&#34;the-answer-encryption&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;The Answer: encryption&lt;/h1&gt;
&lt;p&gt;The answer lies is encryption. The point of the GDPR is that you’re not allowed to leak data - you’re not allowed to give access to others. This doesn’t mean you can’t send them the datafile.&lt;/p&gt;
&lt;p&gt;Somewhat counterintuitively, it is fully consistent with the GDPR to send datafiles with personal data to anybody you want.&lt;/p&gt;
&lt;p&gt;The crux is that as long as they are unable to access the data, it’s as if you didn’t share them.&lt;/p&gt;
&lt;p&gt;And as long as a file is encrypted with an algorithm that is virtually uncrackable, they inevitable require the password to access the data. Without the password, the file is useless and can safely be made public - by making the encrypted datafile public, you are not making the data contained therein public.&lt;/p&gt;
&lt;/div&gt;
&lt;div id=&#34;a-workflow&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;A workflow&lt;/h1&gt;
&lt;p&gt;The simplest workflow is, of course, to avoid collecting personal data. In such cases, simply publish your data once you have it using e.g. GitLab, GitHub, an/or OSF. And in most cases, it is possible to redesign a study such that collecting personal data can be avoided, for example using the Research Code (&lt;a href=&#34;https://researchcode.eu&#34; class=&#34;uri&#34;&gt;https://researchcode.eu&lt;/a&gt;).&lt;/p&gt;
&lt;p&gt;However, in some cases, such redesign is impossible. In those cases, I recommend the following workflow. In this workflow, I assume you already have secure protocols for the data collection itself, and the handling of the data up until the point where all data are merged and one big file exists. However, the workflow below is easily extendible to other scenarios.&lt;/p&gt;
&lt;ol style=&#34;list-style-type: decimal&#34;&gt;
&lt;li&gt;Make sure only a limited number of people have access to the raw, identifiable dataset. Avoid common cloud services such as Dropbox and Google Drive; instead opt for Zero Knowledge solutions such as &lt;a href=&#34;https://sync.com&#34;&gt;Sync.com&lt;/a&gt;.&lt;/li&gt;
&lt;li&gt;If you use advanced versioning systems such as git, make sure the raw, identifiable file is not synced to the server, for example by including “&lt;code&gt;[PRIVATE]&lt;/code&gt;” in the filename, and including a line that contains “&lt;code&gt;\\[PRIVATE]&lt;/code&gt;” (both without the double quotes) in your &lt;code&gt;.gitignore&lt;/code&gt; file to exclude all files and directories with [PRIVATE] in their name (for more about a git-based research workflow, see &lt;a href=&#34;https://sciencer.netlify.com/2019/04/a-reproducible-research-workflow-gitlab-r-markdown-and-open-science-framework/&#34;&gt;this post&lt;/a&gt;).&lt;/li&gt;
&lt;li&gt;Once the data are complete, run a script to anonymize the data and write the anonymized version to a different filename; this publishable file can then be sycned to GitLab or GitHub (and then synced with OSF).&lt;/li&gt;
&lt;li&gt;After you ran the script, archive the raw, identifiable dataset using the Free/Libre and Open Source Software (FLOSS) 7-Zip (see &lt;a href=&#34;https://www.7-zip.org/&#34; class=&#34;uri&#34;&gt;https://www.7-zip.org/&lt;/a&gt; and download the version for your operating system).&lt;/li&gt;
&lt;li&gt;When archiving, choose the .7z format (better compression) or the .zip format (native support in many operating systems) and choose AES-256 encryption (see e.g. &lt;a href=&#34;https://crypto.stackexchange.com/questions/46559/what-are-the-chances-that-aes-256-encryption-is-cracked&#34;&gt;here&lt;/a&gt;, &lt;a href=&#34;https://www.thesslstore.com/blog/what-is-256-bit-encryption/&#34;&gt;here&lt;/a&gt;, and &lt;a href=&#34;https://www.quora.com/Was-AES-256-cracked-or-not&#34;&gt;here&lt;/a&gt;).&lt;/li&gt;
&lt;li&gt;Make sure the password used to encrypt the file is very strong, and store it in a password Manager such as the excellent KeePass2 (see &lt;a href=&#34;https://keepass.info&#34;&gt;the website&lt;/a&gt; and this &lt;a href=&#34;https://www.intigriti.com/public/project/keepass/keepassbyec%5D&#34;&gt;EU project where money is offered for people finding bugs&lt;/a&gt;. Make sure all researchers with access to the raw data (see 1) use such a password manager to store the password.&lt;/li&gt;
&lt;li&gt;Send the password to those people using a secure messaging app such as Signal (see &lt;a href=&#34;https://signal.org/&#34;&gt;https://signal.org/&lt;/a&gt;; conveniently, it also has a desktop client). Do not use insecure channels such as email, and avoid potentially unsafe channels such as WhatsApp, for sending passwords.&lt;/li&gt;
&lt;li&gt;Delete the raw, identifiable dataset. If you want to be entirely sure it can never be recovered, use a program such as &lt;a href=&#34;http://www.fileshredder.org/&#34;&gt;File Shredder&lt;/a&gt; to make sure it gets permanently deleted.&lt;/li&gt;
&lt;li&gt;You can now make both the encrypted dataset and the anonymized dataset public. Both are GDPR-compliant; the encrypted version is only accessible to people with the password, and the anonymized version does not contain personal data. If anybody else should be granted access to the data, you only need to refer them to the repository where you publish your resources (e.g. OSF, GitLab, GitHub, etc), and then (&lt;em&gt;securely!&lt;/em&gt;) send them the password.&lt;/li&gt;
&lt;/ol&gt;
&lt;/div&gt;
&lt;div id=&#34;conclusion&#34; class=&#34;section level1&#34;&gt;
&lt;h1&gt;Conclusion&lt;/h1&gt;
&lt;p&gt;I hope this suggested workflow helps people dealing with personal data. If this was useful to you: I outline another workflow that I found useful in &lt;a href=&#34;https://sciencer.netlify.com/2019/04/a-reproducible-research-workflow-gitlab-r-markdown-and-open-science-framework/&#34;&gt;“A reproducible research workflow: GitLab, R Markdown, and Open Science Framework”&lt;/a&gt;.&lt;/p&gt;
&lt;/div&gt;
</description>
    </item>
    
  </channel>
</rss>
