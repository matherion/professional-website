+++
widget = "blank"  # See https://sourcethemes.com/academic/docs/page-builder/
headless = true  # This file represents a page section.
active = true  # Activate this widget? true/false
weight = 40  # Order that this section will appear.

title = "Books"
subtitle = ""

[design]
  # Choose how many columns the section has. Valid values: 1 or 2.
  columns = "1"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "navy"
  
  # Background gradient.
  # gradient_start = "DarkGreen"
  # gradient_end = "ForestGreen"
  
  # Background image.
  # image = "image.jpg"  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.
  # image_size = "cover"  #  Options are `cover` (default), `contain`, or `actual` size.
  # image_position = "center"  # Options include `left`, `center` (default), or `right`.
  # image_parallax = true  # Use a fun parallax-like fixed background effect? true/false
  
  # Text color (true=light or false=dark).
  text_color_light = false

[design.spacing]
  # Customize the section spacing. Order is top, right, bottom, left.
  padding = ["20px", "0", "20px", "0"]

[advanced]
 # Custom CSS. 
 css_style = ""
 
 # CSS class.
 css_class = ""
+++

Gjalt-Jorn is involved in a number of living<sup>1</sup> Open Access books. These are written in [Bookdown](https://bookdown.org/yihui/bookdown/), an excellent R package by the awesome [Yihui Xie](https://yihui.org/)<sup>2</sup>. They are hosted at [GitLab](https://gitlab.com) (GitLab is a Free/Libre Open Source Software alternative to the proprietary GitHub) Pages. These books are:

- [**The Book of Behavior Change**](https://bookofbehaviorchange.com): this book is a practical handbook for developing behavior change interventions. It does not cover the entire intervention development process, nor provide much theoretical background, instead providing practical tools. It is based on what are basically simple (trivial, even) but inevitable mechanics based on how the human psychology and behavior change work. As such, the book can be used alongside whichever approach one uses, be it Intervention Mapping, the Behavior Change Wheel, Design Thinking, or what have you.

- [**The ROCK book**](https://rockbook.org): The Reproducible Open Coding Kit book, or the ROCK book, describes the Reproducible Open Coding Kit, a versatile open standard for embedding codes in qualitative data (well, as long as the data are textual, e.g. interview transcripts). The ROCK book also describes the `rock` R package, to analyse sources coded with the ROCK standard, and the iROCK interface, to easily code qualitative sources with the ROCK.

- [**Rosetta Stats**](https://rosettastats.com): Rosetta Stats is a statistics chrestomathy. It illustrates how common analyses can be conducted in a variety of statistical software packages (such as SPSS, R, and jamovi). We are also working on a Rosetta Stats Pro: SPSS to R companion, providing more extensive information to support researchers who wish to switch from the expensive and somewhat outdated SPSS to the Free/Libre Open Source Software R, which allows one to, for example, compute Cohen's $d$.

<div style="font-size: 80%"><p>
<sup>1</sup> Living books are books that can be updated 'live', instead of requiring discontinuous release through formal editions. Each of these books will, however, get assigned formal editions every time substantial revisions have taken place compared to the previous version. The first formal edition will mark the first version of the book that is considered finished, and from that point onwards, the DOI of each book will be updated so that each new edition will have a unique DOI.</p>
<p><sup>2</sup> Yihui also wrote the [Blogdown](https://bookdown.org/yihui/blogdown/) package used to create this website with [Hugo](https://gohugo.io/) and the [Academic](https://sourcethemes.com/academic/) theme.</p>
</div>

