+++
# Hero widget.
widget = "hero"  # Do not modify this line!
active = true  # Activate this widget? true/false
weight = 10  # Order that this section will appear.

title = "Gjalt-Jorn Peters' website"

# Hero image (optional). Enter filename of an image in the `static/img/` folder.
hero_media = "rotated-g.svg"

[design.background]
  # Apply a background color, gradient, or image.
  #   Uncomment (by removing `#`) an option to apply it.
  #   Choose a light or dark text color by setting `text_color_light`.
  #   Any HTML color name or Hex value is valid.

  # Background color.
  # color = "white"
  
  # Background gradient.
  # gradient_start = "#4bb4e3"
  # gradient_end = "#2b94c3"
  
  # Background image.
  # image = ""  # Name of image in `static/img/`.
  # image_darken = 0.6  # Darken the image? Range 0-1 where 0 is transparent and 1 is opaque.

  # Text color (true=light or false=dark).
  text_color_light = false

# Call to action links (optional).
#   Display link(s) by specifying a URL and label below. Icon is optional for `[cta]`.
#   Remove a link/note by deleting a cta/note block.
#[cta]
#  url = "tutorial/"
#  label = "Get Started"
#  icon_pack = "fas"
#  icon = "download"

#[cta_alt]
#  url = "https://sourcethemes.com/academic/"
#  label = "View Documentation"

# Note. An optional note to show underneath the links.
#[cta_note]
#  label = ""
#'<a id="academic-release" href="https://sourcethemes.com/academic/updates" data-repo="gcushen/hugo-academic">Latest release <!-- V --></a>'
+++

This is the website of Gjalt-Jorn Peters. It is the home of [his blog ScienceR](#posts), and collects links to other websites.

