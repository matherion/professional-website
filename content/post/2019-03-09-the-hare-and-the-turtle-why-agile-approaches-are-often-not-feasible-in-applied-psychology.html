---
title: 'The hare and the turtle: why agile approaches are often not feasible in applied psychology'
author: Gjalt-Jorn Peters
date: '2019-03-09'
slug: the-hare-and-the-turtle-why-agile-approaches-are-often-not-feasible-in-applied-psychology
categories: []
tags: []
editor_options: 
  chunk_output_type: console
---



<pre><code>## Registered S3 method overwritten by &#39;GGally&#39;:
##   method from   
##   +.gg   ggplot2</code></pre>
<pre><code>## Registered S3 methods overwritten by &#39;lme4&#39;:
##   method                          from
##   cooks.distance.influence.merMod car 
##   influence.merMod                car 
##   dfbeta.influence.merMod         car 
##   dfbetas.influence.merMod        car</code></pre>
<pre><code>## Registered S3 methods overwritten by &#39;userfriendlyscience&#39;:
##   method                     from
##   print.associationMatrix    ufs 
##   print.confIntOmegaSq       ufs 
##   print.confIntV             ufs 
##   print.CramersV             ufs 
##   print.dataShape            ufs 
##   print.descr                ufs 
##   print.ggProportionPlot     ufs 
##   print.meanConfInt          ufs 
##   print.multiVarFreq         ufs 
##   print.normalityAssessment  ufs 
##   print.regrInfluential      ufs 
##   print.scaleDiagnosis       ufs 
##   print.scaleStructure       ufs 
##   print.scatterMatrix        ufs 
##   pander.associationMatrix   ufs 
##   pander.dataShape           ufs 
##   pander.descr               ufs 
##   pander.normalityAssessment ufs 
##   grid.draw.ggProportionPlot ufs</code></pre>
<p>Recently, a case study was published where the usefulness of agile methods in developing behavior change interventions was explored (Ploos van Amstel, Heemskerk, Renes &amp; Hermsen, 2017, <a href="doi:10.1080/14606925.2017.1353014" class="uri">doi:10.1080/14606925.2017.1353014</a>). Although such approaches can have an added value, in the context of applied psychology this added value can quickly become a dangerous pitfall. In this brief blog post I explain why, and how to avoid it.</p>
<div id="basics-of-agile-approaches" class="section level1">
<h1>Basics of Agile approaches</h1>
<p>Agile approaches first became popular in software development, quickly gaining ground after publication of the ‘Manifesto for Agile Software Development’ published in 2001 (<a href="http://agilemanifesto.org/" class="uri">http://agilemanifesto.org/</a>). Where ‘traditional’ methods are often linear, characterised by a phase of thorough planning that is followed by a phase of executing those plans, Agile methods denounce such meticulous planning in favour of an iterative approach, where the execution starts almost immediately based on a first rough draft. The product is then evaluated and adjusted in cycles that quickly succeed each other.</p>
</div>
<div id="agile-behavior-change" class="section level1">
<h1>Agile behavior change</h1>
<p>Applied to behavior change, agile methods prompt rapid development of a first working version of an intervention. This means that common preliminary steps, such as conducting a needs assessment and doing literature research and qualitative and quantitative research to identify the most important determinants of the target behavior, are often foregone. This first working version is then evaluated, and on the basis of this evaluation, it can be improved into a next version. This process can be repeated as resources (time and money) allow.</p>
<p>To enable this, you need to know what to improve. This means you need to collect information about which aspects of your intervention work and which don’t. For example, you could measure the effect it has on a set of determinants that might be important for the target behavior. Alternatively, you need to have an estimate of the intervention’s effect on behavior, and then use an approach akin to the ‘A/B tests’ used in online persuasion to compare multiple alternative interventions to each other.</p>
<p>At first glance, this can seem like a feasible, even exciting approach. It certainly more action-driven and ‘sexy’ that the conventional approaches to behavior change, which require comprehensive canvassing of the literature, talking to your target population extensively, then verifying the preliminary results in quantitative research, and then using complicated-sounding approaches such as Confidence Interval-Based Estimation of Relevance to select the determinants to target (see e.g. <a href="doi:10.3389/fpubh.2017.00165" class="uri">doi:10.3389/fpubh.2017.00165</a>).</p>
<p>However…</p>
</div>
<div id="the-pitfall" class="section level1">
<h1>The pitfall</h1>
<p>The pitfall of agile methods as employed in applied psychology relates to the sampling distribution of effect sizes. Decisions that are taken in between the iterations that are central to agile approaches are based on the evaluation of the intervention. To enable <em>improving</em> the intervention in these successive iterations, these decisions require learning about the intervention’s effectiveness from the last iteration. This is inevitably a matter of causality, and therefore, requires an experimental design.</p>
<p>The effect size commonly used in experimental designs is Cohen’s <span class="math inline">\(d\)</span>. Cohen’s <span class="math inline">\(d\)</span>’s sampling distribution is very wide compared to that of, for example, Pearson’s <span class="math inline">\(r\)</span> (see <a href="doi:10.31234/osf.io/cjsk2" class="uri">doi:10.31234/osf.io/cjsk2</a>). Because one image is worth a thousand words, behold Figure 1.</p>
<!--\@ref(fig:cohens-d-sampling-dist1).-->
<pre class="r"><code>attr(ufs::cohensdCI(d=0.5, n=128, plot=TRUE),
                      &#39;plot&#39;) +
  ggplot2::xlab(&quot;Cohen&#39;s d&quot;) +
  ggplot2::ylab(&quot;Density&quot;) +
  ggplot2::ggtitle(&quot;Sampling distribution for Cohen&#39;s d&quot;) +
  ggplot2::theme_bw(base_size = 15) +
  ggplot2::theme(axis.title.x.top = element_blank());</code></pre>
<div class="figure"><span id="fig:cohens-d-sampling-dist1"></span>
<img src="/post/2019-03-09-the-hare-and-the-turtle-why-agile-approaches-are-often-not-feasible-in-applied-psychology_files/figure-html/cohens-d-sampling-dist1-1.png" alt="Cohen's d's sampling distribution for a moderate population effect size (d = 0.5) and for a 2-cell design with 80\% power (i.e. 64 participants per group)." width="672" />
<p class="caption">
Figure 1: Cohen’s d’s sampling distribution for a moderate population effect size (d = 0.5) and for a 2-cell design with 80% power (i.e. 64 participants per group).
</p>
</div>
<p>This figure shows how wide the sampling distribution of Cohen’s <span class="math inline">\(d\)</span> is in an experiment with a 2-cell design and 64 participants in each group (i.e. the required sample size to achieve 80% power in a null hypothesis significance testing situation). In this case, obtaining a Cohen’s <span class="math inline">\(d\)</span> of 0.20 of 0.25 is very likely. If a Cohen’s <span class="math inline">\(d\)</span> of 0.5 is obtained, this means that population values of 0.25 is quite plausible; if a Cohen’s <span class="math inline">\(d\)</span> of .25 is obtained, that means that a <em>negative</em> population value for Cohen’s <span class="math inline">\(d\)</span> is quite likely. For example, let us assume the first iteration of an intervention has a small effect of (Cohen’s <span class="math inline">\(d\)</span> = 0.2). In that case, this is the sampling distribution it comes from:</p>
<pre class="r"><code>attr(ufs::cohensdCI(d=.2, n=128, plot=TRUE),
                      &#39;plot&#39;) +
  ggplot2::xlab(&quot;Cohen&#39;s d&quot;) +
  ggplot2::ylab(&quot;Density&quot;) +
  ggplot2::ggtitle(&quot;Sampling distribution for Cohen&#39;s d&quot;) +
  ggplot2::theme_bw(base_size = 15) +
  ggplot2::theme(axis.title.x.top = element_blank());</code></pre>
<div class="figure"><span id="fig:cohens-d-sampling-dist2"></span>
<img src="/post/2019-03-09-the-hare-and-the-turtle-why-agile-approaches-are-often-not-feasible-in-applied-psychology_files/figure-html/cohens-d-sampling-dist2-1.png" alt="Cohen's d's sampling distribution for a small population effect size (d = 0.2) and for a 2-cell design with 80\% power (i.e. 64 participants per group)." width="672" />
<p class="caption">
Figure 2: Cohen’s d’s sampling distribution for a small population effect size (d = 0.2) and for a 2-cell design with 80% power (i.e. 64 participants per group).
</p>
</div>
<p>In every study (every sample), one Cohen’s <span class="math inline">\(d\)</span> value is taken randomly from this distribution. The probability that you’ll get a significant result is very low, of course. But even if you <em>do</em> get a significant result (conform NHST), you’ll still not know how effective your intervention is. The point estimate you obtain in any given sample is useless given how wide this sampling distribution is. Therefore, you’ll need the confidence interval to say something useful about how effective that iteration of the intervention is - but that’s extremely wide. You cannot reasonably assume that the population effect is in the middle of the confidence interval, given that they ‘jump around’ (see e.g. <a href="https://rpsychologist.com/d3/CI/" class="uri">https://rpsychologist.com/d3/CI/</a>).</p>
<p>Therefore, with wide confidence intervals, you will learn nothing from one iteration in your agile behavior change approach. This means that you hvae no information to inform your next iteration. This is especially problematic because unless you do invest in comprehensive planning (effectively taking much of the agile out of your agile approach), the first intervention you develop will have a low effect size (the <span class="math inline">\(d\)</span> = 0.2 is not unreasonable in that case).</p>
</div>
<div id="avoiding-the-pitfall" class="section level1">
<h1>Avoiding the pitfall</h1>
<p>There is one way to avoid this pitfall: plan your data collection for each iteration such that you obtain sufficiently narrow confidence intervals so that you are able to make informed decisions, thereby enabling iterate towards more effective interventions.</p>
<p>In our preprint about planning for Cohen’s <span class="math inline">\(d\)</span> confidence intervals (<a href="doi:10.31234/osf.io/cjsk2" class="uri">doi:10.31234/osf.io/cjsk2</a>), we introduce functions and provide tables to plan for specific confidence intervals widths. For 95% confidence intervals, this is one of those tables:</p>
<pre class="r"><code>table1 &lt;- sapply(widths, function(width) {
  return(sapply(cohensDvalues, function(cohensD) {
    return(ufs::pwr.cohensdCI(d=cohensD, w=width, conf.level=.95, silent=TRUE));
  }));
});
rownames(table1) &lt;- cohensDvalues;
colnames(table1) &lt;- widths;
knitr::kable(table1);</code></pre>
<table>
<caption>Table 1: The required sample sizes for confidence intervals of varying widths with 95% confidence.</caption>
<thead>
<tr class="header">
<th align="left"></th>
<th align="right">0.05</th>
<th align="right">0.1</th>
<th align="right">0.15</th>
<th align="right">0.2</th>
<th align="right">0.25</th>
<th align="right">0.3</th>
<th align="right">0.35</th>
<th align="right">0.4</th>
<th align="right">0.45</th>
<th align="right">0.5</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td align="left">0.2</td>
<td align="right">6178</td>
<td align="right">1545</td>
<td align="right">687</td>
<td align="right">387</td>
<td align="right">248</td>
<td align="right">172</td>
<td align="right">127</td>
<td align="right">97</td>
<td align="right">77</td>
<td align="right">62</td>
</tr>
<tr class="even">
<td align="left">0.5</td>
<td align="right">6339</td>
<td align="right">1585</td>
<td align="right">705</td>
<td align="right">397</td>
<td align="right">254</td>
<td align="right">177</td>
<td align="right">130</td>
<td align="right">100</td>
<td align="right">79</td>
<td align="right">64</td>
</tr>
<tr class="odd">
<td align="left">0.8</td>
<td align="right">6639</td>
<td align="right">1660</td>
<td align="right">738</td>
<td align="right">416</td>
<td align="right">266</td>
<td align="right">185</td>
<td align="right">136</td>
<td align="right">104</td>
<td align="right">83</td>
<td align="right">67</td>
</tr>
</tbody>
</table>
<p>As this table makes clear, 95% confidence intervals for Cohen’s <span class="math inline">\(d\)</span> only start to become narrower with 200 participants per group. In fact, with 200 participants per group, this is your sampling distribution if the Cohen’s <span class="math inline">\(d\)</span> for your intervention has a population value of .2:</p>
<pre class="r"><code>attr(ufs::cohensdCI(d=.2, n=400, plot=TRUE),
                      &#39;plot&#39;) +
  ggplot2::xlab(&quot;Cohen&#39;s d&quot;) +
  ggplot2::ylab(&quot;Density&quot;) +
  ggplot2::ggtitle(&quot;Sampling distribution for Cohen&#39;s d&quot;) +
  ggplot2::theme_bw(base_size = 15) +
  ggplot2::theme(axis.title.x.top = element_blank());</code></pre>
<div class="figure"><span id="fig:cohens-d-sampling-dist3"></span>
<img src="/post/2019-03-09-the-hare-and-the-turtle-why-agile-approaches-are-often-not-feasible-in-applied-psychology_files/figure-html/cohens-d-sampling-dist3-1.png" alt="Cohen's d's sampling distribution for a small population effect size (d = 0.2) and for a 2-cell design with 200 participants per group." width="672" />
<p class="caption">
Figure 3: Cohen’s d’s sampling distribution for a small population effect size (d = 0.2) and for a 2-cell design with 200 participants per group.
</p>
</div>
<p>As you see, in that case, it’s still quite likely that you’ll end up with a Cohen’s <span class="math inline">\(d\)</span> that is very close to zero. You’ll need to get to a total of 1500 participants to get an accurate estimate:</p>
<pre class="r"><code>attr(ufs::cohensdCI(d=.2, n=1500, plot=TRUE),
                      &#39;plot&#39;) +
  ggplot2::xlab(&quot;Cohen&#39;s d&quot;) +
  ggplot2::ylab(&quot;Density&quot;) +
  ggplot2::ggtitle(&quot;Sampling distribution for Cohen&#39;s d&quot;) +
  ggplot2::theme_bw(base_size = 15) +
  ggplot2::theme(axis.title.x.top = element_blank());</code></pre>
<div class="figure"><span id="fig:cohens-d-sampling-dist4"></span>
<img src="/post/2019-03-09-the-hare-and-the-turtle-why-agile-approaches-are-often-not-feasible-in-applied-psychology_files/figure-html/cohens-d-sampling-dist4-1.png" alt="Cohen's d's sampling distribution for a small population effect size (d = 0.2) and for a 2-cell design with 750 participants per group." width="672" />
<p class="caption">
Figure 4: Cohen’s d’s sampling distribution for a small population effect size (d = 0.2) and for a 2-cell design with 750 participants per group.
</p>
</div>
<p>So, for your first iteration to be more or less informative and allow you to improve the intervention in the right direction in the second iteration, you would need around 1500 participants. That’s a lot though - especially given that participants are a scarce resource that you shouldn’t lightly ‘use up’. Even if you can, collecting so much data can cost a lot of time, making it hard to fit in an agile workflow.</p>
</div>
<div id="scenarios-where-agile-approaches-are-feasible" class="section level1">
<h1>Scenarios where agile approaches are feasible</h1>
<p>Of course, agile approaches can still be feasible in some situations. Specifically, situations where you can collect data on interventions’ effectiveness without actually requiring <em>participants</em> - instead using publicly available data, for example, by coding people’s behavior in a public space.</p>
<p>However, this narrow field of application is important to keep in mind. If you work with a behavior that cannot be observed in public spaces, you cannot compare alternative initial interventions using an A/B-test like approach. Also, when you want to base the iterative intervention improvements on which components work better and which work worse, you will need to measure your participants’ determinants. In such cases, agile approaches are only feasible if you can afford to test thousands of participants.</p>
<p>In such scenario’s, it quickly becomes faster and cheaper to invest more time and effort in the planning phase and forego an agile approach. An additional complication may be ethical approval - how ethical is it to consume hundreds or thousands of participant-hours more than is necessary if you use another route?</p>
</div>
<div id="final-notes" class="section level1">
<h1>Final notes</h1>
<p>I did not write this blog post because I think agile approaches are not useful or valuable; instead, I wrote this because I know that most people don’t normally think from the perspective of sampling distributions, and that can easily lead people to erreoneously believe that agile approaches are widely applicable.</p>
<p>A second remark is that these dynamics (the number of participants, or more accurately, data points, required to obtain more or less stable effect size estimates) do not specifically plague agile approaches. This applies to all research: also, for example, effect evaluations of interventions. If you find this interesting, see our preprint at <a href="doi:10.31234/osf.io/cjsk2" class="uri">doi:10.31234/osf.io/cjsk2</a>.</p>
</div>
