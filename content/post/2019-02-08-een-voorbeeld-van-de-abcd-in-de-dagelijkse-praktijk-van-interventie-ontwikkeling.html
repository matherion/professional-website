---
title: Een voorbeeld van de ABCD in de dagelijkse praktijk van interventie-ontwikkeling
author: Gjalt-Jorn Peters
date: '2019-02-08'
slug: een-voorbeeld-van-de-abcd-in-de-dagelijkse-praktijk-van-interventie-ontwikkeling
categories: []
tags: []
---



<p>De Acyclic Behavior Change Diagram, oftewel ABCD, is een nuttig hulpmiddel bij het planmatig (of eigenlijk ook bij het niet zo planmatig) ontwikkelen van een gedragsveranderingsinterventie.<a href="#fn1" class="footnote-ref" id="fnref1"><sup>1</sup></a> In dit voorbeeld werk ik uit hoe dit in de dagelijkse praktijk in zijn werk zou gaan.</p>
<p>Er staat meer uitleg over ABCDs in de <a href="https://academy-of-behavior-change.github.io/behaviorchange/articles/abcd-laagdrempelige_nederlandse_uitleg.html">ABCD: Laagdrempelige Nederlandse uitleg</a>.<a href="#fn2" class="footnote-ref" id="fnref2"><sup>2</sup></a> Als je die nog niet hebt gelezen, is het handig om daar te starten. Deze post richt zich vooral op het praktische gebruik van ABCDs. Verder verwijs ik soms naar Intervention Mapping terminologie. De ABCD helpt vooral bij stap 3 van Intervention Mapping; als je je Matrices van Change Objectives uit stap 2 van Intervention Mapping al hebt, dan heb je zelfs het grootste deel van de ABCD al klaar (zie stukje “De ABCD en de Matrix of Change Objectives” hieronder).</p>
<div id="voorbereiding-spreadsheet-aanmaken" class="section level1">
<h1>Voorbereiding: Spreadsheet aanmaken</h1>
<p>ABCDs werken vanaf een spreadsheet. Ik vind het het makkelijkst om een Google Sheet te gebruiken, omdat je daarin handig kunt samenwerken. De Google Sheet van het voorbeeld in de <a href="https://academy-of-behavior-change.github.io/behaviorchange/articles/abcd-laagdrempelige_nederlandse_uitleg.html">Nederlandse uitleg</a> staat op <a href="https://docs.google.com/spreadsheets/d/1EKVqtG1kmf0ZxEvFUOBqUmakXTd9Ye3aSyZucBqiyRM/edit#gid=0" class="uri">https://docs.google.com/spreadsheets/d/1EKVqtG1kmf0ZxEvFUOBqUmakXTd9Ye3aSyZucBqiyRM/edit#gid=0</a> - deze kun je kopieren door links bovenin op ‘File’ of ‘Bestand’ te klikken en dan ‘Make a copy’ of ‘Maak een kopie’ te kiezen.</p>
<p>In plaats van een Google Sheet kun je ook een spreadsheet gebruiken in een ander programma, bijvoorbeeld Microsoft Excel of LibreOffice Calc, en ofwel de kolomnamen (of hele kolommen) uit het voorbeeld kopiëren, ofwel zelf namen intypen. De namen van de kolommen maken niet, maar de volgorde wel. De kolommen zijn, van links naar rechts:</p>
<ol style="list-style-type: decimal">
<li>Gedragsveranderingsprincipes (Behavior Change Principles of BCPs, ook wel ‘methoden voor gedragsverandering’ in Intervention Mapping, Behavior Change Techniques in het jargon van het Britse ‘Behavior Change Wheel’, of soms ook wel ‘actieve ingredienten’ genoemd): dit zijn de psychologische principes waardoor een interventie iets kan veranderen in het hoofd van de doelgroep.</li>
<li>Voorwaarden voor effectiviteit: dit zijn de voorwaarden waaraan de implementatie van de BCPs moet voldoen willen die effectief kunnen zijn bij het beinvloedden van de doelpopulatie.</li>
<li>Toepassingen: dit zijn de specifieke toepassingen van de BCPs. Waar de BCPs theoretische en abstract zijn, zijn de toepassingen concreet en tastbaar.</li>
<li>Subdeterminanten (‘Change Objectives’ in Intervention Mapping; ook wel ‘opvattingen’ of ‘beliefs’ genoemd): dit zijn de specifieke aspecten van de menselijke psychologie waar een interventie zich op richt.</li>
<li>Determinanten: dit zijn de clusters van subdeterminanten die op elkaar lijken (of functioneel op elkaar lijken; dus op dezelfde manier werken), die in psychologische theorieen worden onderscheiden, en die het mogelijk maken om bij specifieke subdeterminanten op te zoeken welke BCPs gebruikt kunnen worden om die te veranderen).</li>
<li>Subgedragingen (‘Performance Objectives’ in Intervention Mapping; de specifieke subgedragingen waaruit het doelgedrag bestaat.</li>
<li>Doelgedrag: het wenselijke gedrag waar de interventie zich op richt.</li>
</ol>
</div>
<div id="stap-1-doelgedrag" class="section level1">
<h1>Stap 1: Doelgedrag</h1>
<p>Het heeft geen zin een interventie te ontwikkelen als niet duidelijk is wat het doelgedrag is. Je start dus met het invullen van de ABCD specificatie aan de rechterkant. Je vult daar het doelgedrag in. Zoals mensen die bekend zijn met Intervention Mapping (IM) al weten is het doelgedrag altijd een wenselijk gedrag: het is niet verstandig om een interventie te ontwikkelen waarbij het doel niet is om een bepaald gedrag te bereiken. Als de needs assessment (IM stap 1) uitwijst dat er een onwenselijk gedrag is dat moet veranderen (bijvoorbeeld: XTC-gebruikers gebruiken een te hoge dosis MDMA), bepaal dan eerst wat mensen precies moeten doen.</p>
<p>Lukt het niet om te bepalen wat mensen precies moeten doen? Dat is indicatief voor het onvoldoende begrijpen van de situatie. Dan moet je terug naar stap 1 van IM totdat je hier wel voldoende inzicht in hebt.</p>
<p>Als je het doelgedrag helder hebt, documenteer dan ook de onderbouwing van dat doelgedrag, zodat je later altijd kunt uitleggen waarom je dat doelgedrag koos (hoe je weet dat het samenhangt met het (gezondheids)probleem). Zo voorkom je ook dat je een doelgedrag kiest dat logisch lijkt, maar waarbij je bij nader inzien geen reden hebt om aan te nemen dat het uitmaakt voor je uiteindelijke probleem.</p>
<p>Na deze stap 1 heb je in je ABCD specificatie dus in de rechter kolom (de zevende kolom, kolom G in een spreadsheet) je doelgedrag staan. Die cel (cel G2) is op dit punt nog de enige cel die je gevuld hebt (behalve dan de zeven cellen met de kolomnamen natuurlijk, cellen A1-G1).</p>
</div>
<div id="stap-2-subgedragingen" class="section level1">
<h1>Stap 2: Subgedragingen</h1>
<p>Vervolgens denk je na over waar dat doelgedrag eigenlijk uit bestaat. Vaak zijn doelgedragingen geformuleerd op een heel generiek niveau, en omvat dat gedrag, als je er goed naar kijkt, verschillende subgedragingen waarbij mensen andere redenen kunnen hebben om die subgedragingen al dan niet uit te voeren, en/of waarbij bij elk subgedrag andere omgevingsbarrieres een rol kunnen spelen (zie voor het klassieke voorbeeld van condoomgebruik de <a href="https://academy-of-behavior-change.github.io/behaviorchange/articles/abcd.html">Engelstalige uitleg van de ABCD</a>).</p>
<p>Bepaal die subgedragingen, en zorg dat je documenteert waarom je die subgedragingen kiest (hoe je weet dat die belangrijk zijn). Voeg die subgedragingen vervolgens toe in de ABCD specificatie. Als er maar eentje hebt, zet je die dus in de kolom net links van het doelgedrag (cel 2F). Als je er twee hebt, zet je de tweede in de zesde kolom onder de eerste (cel 3F), en als je er drie hebt, zet je de derde daar weer onder.</p>
<p>Zorg ervoor dat achter elke subgedraging het doelgedrag staat; in kolom G staat dus steeds hetzelfde doelgedrag achter elk subgedrag. Als je al een Matrix of Change Objectives hebt, dan zijn staan de subgedragingen als Performance Objectives in de rijen in die matrix; die kun je dan rechtstreeks hierin zetten.</p>
<p>Na stap 2 heb je dus de zesde en zevende kolommen (F en G) deels gevuld, waarbij die informatie een ‘rechthoek’ vormt: er mogen geen lege cellen zijn.</p>
</div>
<div id="stap-3-determinanten" class="section level1">
<h1>Stap 3: Determinanten</h1>
<p>Als je de subgedragingen helder hebt, voeg dan in de vijfde kolome (kolom E) de determinanten van dat subgedrag toe, bijvoorbeeld “Kennis”, “Risicoperceptie”, “Bewustzijn”, “Attitude”, “Descriptieve Normen”, “Injunctieve Normen”, “Self-Efficacy” (of “Eigen-Effectiviteit” ), “Gewoonte”, “Zelf-Identiteit”, enzovoort. Hier kies je natuurlijk de determinanten waarvan uit het determinantenonderzoek bleek dat deze belangrijk zijn.</p>
<p>Als je al een Matrix of Change Objectives hebt, dan zijn staan de determinanten als kolommen in die matrix; die kun je dan rechtstreeks hierin zetten.</p>
<p>Je herhaalt hier weer het truukje uit stap 2: als je voor een subgedrag meerdere determinanten hebt (en dat zit er dik in, want gedrag wordt altijd door meerdere determinanten bepaald - als je er maar eentje hebt, zie je dus gegarandeerd iets over het hoofd), dan kopieer je de cellen in de zesde en zevende kolommen (F eng G) zodat achter elke determinant het bijbehorende subgedrag en doelgedrag staat.</p>
<p>Na stap 3 heb je dus de vijfde, zesde en zevende kolommen (E, F en G) deels gevuld, waarbij die informatie een weer ‘rechthoek’ vormt: er mogen geen lege cellen zijn.</p>
</div>
<div id="stap-4-subdeterminanten" class="section level1">
<h1>Stap 4: Subdeterminanten</h1>
<p>Vervolgens voeg je de specifieke opvattingen toe die binnen die determinanten vallen voor elk subgedrag. Deze subdeterminanten zijn de specifieke aspecten van de menselijke psychologie die elk subgedrag (en dus uiteindelijk het doelgedrag) verklaren. Als je met mensen over hun gedrag praat, is het niveau van de subdeterminanten het niveau waarom mensen over hun gedrag praten: dit gaat over concrete voor- en nadelen, wat mensen wel en niet kunnen, wat mensen denken dat hun omgeving goed- of afkeurt, wat mensen als gewoonte hebben geautomatiseerd, enzovoort.</p>
<p>Hier geldt weer dat je de rijen in kolommen E-G waarschijnlijk moet kopieren, zodat elke subdeterminant in kolom D gelijk gevolgd wordt door de determinant waar die subdeterminant onder valt in kolom E, die dan weer wordt gevolgd door het subgedrag dat die determinant en bijbehorende subdeterminant voorspellen, die dan in kolom G weer wordt gevolgd door het uiteindelijke doelgedrag waar we het allemaal voor doen.</p>
<p>Als je al een Matrix of Change Objectives hebt, dan zijn dit de Change Objectives in de cellen in die matrix; die kun je dan rechtstreeks hierin zetten.</p>
<p>Na stap 4 heb je dus de vierde, vijfde, zesde en zevende kolommen (D, E, F en G) deels gevuld, waarbij die informatie een weer ‘rechthoek’ vormt: er mogen geen lege cellen zijn. Op dit moment heb je eigenlijk je Matrix of Change Objectives gereproduceerd in de ABCD specificatie.</p>
</div>
<div id="de-abcd-en-de-matrix-of-change-objectives" class="section level1">
<h1>De ABCD en de Matrix of Change Objectives</h1>
<p>De rechter vier kolommen van de ABCD specificatie bevatten exact dezelfde informatie als een Matrix of Change Objectives. In de Matrix of Change Objectives staan de subgedragingen in de rijen (elk subgedrag, of Performance Objective, wordt dus benoemd in de eerste kolom); de determinanten in de kolommen (elke determinant wordt dus benoemd in de eerste rij), en de subdeterminanten in de cellen (de Change Objectives). En elke Matrix of Change Objectives gaat over precies ëën doelgedrag.</p>
<p>Als je deze al hebt gemaakt, kun je dus makkelijk dezelfde informatie invullen in kolommen D-G. De informatie in die kolommen vormt wat binnen Intervention Mapping eigenlijk onterecht het Logic Model of Change wordt genoemd. Eigenlijk is het het Logic Model of Explanation: het bevat de informatie die bekend is over waarom mensen doen wat ze doen, maar zegt nog niets over hoe dat vervolgens veranderd kan worden.</p>
<p>De ABCD ‘begint’ eigenlijk pas daarna: die helpt nu juist wel om overzicht te krijgen over hoe die verandering kan worden bereikt. De ABCD helpt dus vooral bij stap 3 van Intervention Mapping: het koppelen van gedragsveranderingsmethoden (BCPs) aan de juiste subdeterminanten, via de bijbehorende determinanten, zodat een interventie kansrijk is.</p>
</div>
<div id="stappen-5-7-gedragsverandering" class="section level1">
<h1>Stappen 5-7: Gedragsverandering</h1>
<p>Hoewel het logisch is om stappen 1-4 in deze volgorde uit te voeren (vanaf doelgedrag, via subgedragingen en determinanten, naar subdeterminanten), is er niet ëën logische volgorde voor het invullen van het linker deel van de ABCD. Dat komt omdat je soms liever start vanuit de ‘actieve ingrediënten’, oftewel, de gedragsveranderingsmethoden of BCPs, en soms liever vanuit de specifieke toepassingen. Ikzelf vind het handiger om op dit punt niet langer van rechts naar links te werken, maar te switchen en in de eerste kolom (A) verder te gaan, dan de derde kolom (C) en dan naar het midden toe te werken, dus die volgorde volg ik hier. Maar ik vermoed dat mensen met veel praktijkervaring met interventie-ontwikkeling het misschien juist handiger vinden om nu eerst concreet over de interventie na te denken, en dus nu de toepassingen in de derde kolom (C) uit te werken.</p>
<p>De volgorde maakt niet uit, als je uiteindelijk maar zorgt dat alle cellen in je ‘rechthoek’ gevuld zijn met iets zinnigs.</p>
</div>
<div id="stap-5-gedragsveranderingsmethoden" class="section level1">
<h1>Stap 5: Gedragsveranderingsmethoden</h1>
<p>Ik zou vervolgens dus op basis van de (sub-)determinanten gaan nadenken over welke methoden voor gedragsverandering (gedragsveranderingsprincipes, BCPs) ik wil toepassen (en kan toepassen binnen de praktische beperkingen waar je binnen moet werken). Op elke regel voeg je dan de betreffende BCP toe. Die BCP kun je bijvoorbeeld halen uit de tabellen in hoofdstuk 6 van het Intervention Mapping boek (pagina’s 345-433 in de vierde editie; de eerste tabel is tabel 6.5, op pagina 376), of uit de tabellen bij het <a href="https://osf.io/ng3xh">Open Access artikel dat je hier vindt</a>.</p>
<p>In stappen 1-4 was het steeds nodig om rijen toe te voegen. Vanaf dit punt is dat waarschijnlijk niet meer nodig, <em>tenzij</em> je je met meerdere BCPs op dezelfde subdeterminant wil richten. In alle andere gevallen zijn stappen 5-7 dus gewoon invuloefeningen om de ABCD specificatie (de ‘rechthoek’) te vullen.</p>
</div>
<div id="stap-6-voorwaarden-voor-effectiviteit" class="section level1">
<h1>Stap 6: Voorwaarden voor effectiviteit</h1>
<p>Als eenmaal duidelijk is welke BCPs gebruikt gaan worden in de interventie, kunnen de bijbehorende parameters voor effectiviteit worden toegevoegd in de cel ernaast in de tweede kolom (kolom B; zie voor waarom deze parameters voor effectiviteit zo cruciaal zijn <a href="https://academy-of-behavior-change.github.io/behaviorchange/articles/abcd-laagdrempelige_nederlandse_uitleg.html">deze uitleg van ABCDs</a>). Hier kunnen bovendien overwegingen met betrekking tot de doelgroep, cultuur, context, enzovoorts worden toegevoegd.</p>
</div>
<div id="stap-7-toepassingen" class="section level1">
<h1>Stap 7: Toepassingen</h1>
<p>In de derde kolom, kolom C, staan de toepassingen. Net als de subdeterminanten de concrete, min of meer tastbare, relevante gevoelens en overwegingen zijn die een rol spelen bij het doelgedrag, zijn de toepassingen de concrete onderdelen van de interventie of campagne. Dit kunnen dus (delen van) protocollen voor gespreksvoering zijn, of (delen van) een poster, of (delen van) een filmpje, of (delen van) een app, of (delen van) een comic, of (delen van) een spel, enzovoorts.</p>
<p>Deze kolom C is waar mensen soms ten onrechte gelijk mee beginnen als ze een interventie ontwikkelen. Het is ook een belangrijke kolom: hierin staan immers de concrete producten die samen de interventie (of campagne) vormen. Tegelijkertijd kunnen die toepassingen alleen maar werken als ze 1) zich richten op subdeterminanten die ook echt belangrijk zijn voor het doelgedrag, en 2) dat doen door middel van gedragsveranderingsprincipes die ook echt werken voor die subdeterminanten. De waarde van zo’n toepassing hangt dus af van of er klopt wat er links (kolommen A en B) en rechts (kolommen D, E, F en G) van de toepassing staat. Die aannames over waarom mensen doen wat ze doen (kolommen D, E, F en G) en over hoe we mensen dingen kunnen leren (kolommen A en B) bepalen immers of een interventie kans van slapen kan hebben.</p>
</div>
<div id="check-op-compleetheid-en-een-truukje-als-je-nog-niet-klaar-bent" class="section level1">
<h1>Check op compleetheid en een truukje als je nog niet klaar bent</h1>
<p>Als alles in ingevuld is het belangrijk om nog even te checken dat de ABCD specificatie volledig is. Er mogen dus geen lege cellen zijn; er moet een volledig gevulde ’rechthoek zijn ontstaan.</p>
<p>Als je sommige dingen nog niet zeker weet, vul dan tijdelijk iets in als “toepassing 1” of “BCP 1”, zodat je wel alvast de ABCD kunt genereren, als je dat halverwege alvast wil doen.</p>
</div>
<div id="het-genereren-van-de-abcd-de-diagram-zelf" class="section level1">
<h1>Het genereren van de ABCD: de diagram zelf</h1>
<p>Om de ABCD te genereren kun je naar <a href="https://a-bc.eu/apps/abcd" class="uri">https://a-bc.eu/apps/abcd</a> gaan. Hier draait een zogenaamde “R Shiny app” waar je de URL naar je Google Sheet kunt copy-pasten. Dit werkt alleen als de Google Sheet openbaar toegankelijk is (blauwe “Share” of “Delen” knop rechts bovenin), én als hij gepubliceerd is op het web (“File” of “Bestand” menu, dan “Publish to the web” of “Publiceren naar het web”, dan bevestigen in het dialoogje dat verschijnt). Als je dit hebt gedaan kun je de URL uit de adresbalk van je browser pakken en in het veld op <a href="https://a-bc.eu/apps/abcd" class="uri">https://a-bc.eu/apps/abcd</a> copy-pasten.</p>
<p>Wat je ook kunt doen, bijvoorbeeld als je met Microsoft Excel of LibreOffice Calc werkt, of als je je Google Sheet niet openbaar wil maken, is je ABCD specificatie exporteren naar CSV (‘comma separated values’), en deze dan importeren.</p>
<p>De app laat je vervolgens de geïmporteerde tabel zien, waarna je op basis van die informatie de ABCD kunt genereren. Die kun je vervolgens downloaden en lokaal op je PC opslaan.</p>
<p>Overigens werkt de app het beste in Google Chrome; het is dus handig om die te gebruiken.</p>
</div>
<div id="het-nut-van-de-abcd" class="section level1">
<h1>Het nut van de ABCD</h1>
<p>Als je deze werkwijze volgt, heeft dat een aantal voordelen:</p>
<ol style="list-style-type: decimal">
<li>Je houdt overzicht:</li>
<li>over welk gedrag je het nu precies hebt (kolommen F en G);</li>
<li>over wat je weet over waarom mensen doen wat ze doen (kolommen D en E);</li>
<li>over of de toepassingen in je interventie wel alles afdekken wat belangrijk is voor waarom mensen doen wat ze doen (kolommen C en D);</li>
<li>over of er wel een reden is om aan te nemen dat de toepassingen in je interventie effect kunnen hebben (kolommen A en B);</li>
<li>Als je ook consequent documenteert waarom je alles dat je toevoegt, toevoegt, kun je altijd terugvinden en uitleggen waar je aannames op zijn gebaseerd;</li>
<li>Je kunt makkelijk je aannames die ten grondslag liggen aan de interventie aan elkaar presenteren en bespreken.</li>
</ol>
<p>Het handigste vind ikzelf dat je de ‘causale ketting’ van gedragsverandering heel expliciet maakt, en het gelijk merkt als je een schakel bent vergeten: als er een cel leeg is in de ABCD specificatie-rechthoek, klopt er iets niet.</p>
<p>Je wordt dus gedwongen om overal over na te denken, wat natuurlijk de kans op effectiviteit heel erg vergroot.</p>
<p>Een ander nut is dat je over ABCDs makkelijk met elkaar kunt communiceren; als verschillende organisaties bijvoorbeeld met ABCDs werken, kun je makkelijk bij intervisie-bijeenkomsten een serie van elkaars ABCDs bespreken en zo heel efficiënt informatie uitwisselen over effectieve preventie.</p>
</div>
<div class="footnotes">
<hr />
<ol>
<li id="fn1"><p>Voor de duidelijkheid: interventies om bewustzijn of kennis te verbeteren zijn ook gedragsveranderingsinterventies; bewustzijn en kennis worden alleen verbeterd omdat de hoop is dat gedrag zal veranderen. Als van te voren zeker is dat gedrag hetzelfde zal blijven ook al veranderen bewustzijn en/of kennis, dan is investeren in een interventie die zich richt op kennis en/of bewustzijn niet goed te verantwoorden.<a href="#fnref1" class="footnote-back">↩</a></p></li>
<li id="fn2"><p>Dit ‘laagdrempelijk’ is misschien wat optimistisch - ik vind het lastig om in te schatten welke achtergrondkennis mensen al hebben. Als je dit helemaal niet laagdrempelig vindt: mijn excuses!<a href="#fnref2" class="footnote-back">↩</a></p></li>
</ol>
</div>
