---
title: 'Collaborating on reproducible manuscripts for dummies: an introduction using
  git, R Markdown and Zotero'
author: Gjalt-Jorn Peters
date: '2019-11-13'
slug: collaborating-on-reproducible-manuscripts-for-dummies-an-introduction-using-git-r-markdown-and-zotero
categories: ["Open Science", "Workflow", "Sci-Ops"]
tags: []
editor_options: 
  chunk_output_type: console
---



<p>This document is an introduction to a workflow that facilitates collaborating with other researchers if your aim is to apply Open Science principles.</p>
<div id="background" class="section level1">
<h1>Background</h1>
<p>This workflow is consistent with the following Open Science principles:</p>
<ul>
<li><p><strong>Being as transparent as possible.</strong> In scientific publications of empirical endeavours, it is important that people can inspect how the results derive from the collected data. One way to achieve this is to include deparate analysis scripts and let the readers (and the future ‘you’) sort things out for themselves. However, this still leaves a lot of room for interpretation. Ideally, every reported result can be traced back unequivocally to analyses and data. Reproducible manuscripts enable you to achieve this with relatively little effort. This will serve your future self, your colleagues, and the scientific community.</p></li>
<li><p><strong>Building an Open infrastructure for our science.</strong> One of the Open Science foundations is inclusiveness. To build an inclusive science requires a number of things. One is writing tutorials such as this one, so that learning the required competences is in everybody’s reach. Another is to use Free/Libre Open Source Software (FLOSS) and open standards as much as possible, and use proprietary software as little as possible. This is important because one of the many divides between scientists world-wise is financial. Choosing to use proprietary software therefore excludes a part of the scientific community, either now or potentially in the future (in those cases where limited free versions of proprietary software exist).</p></li>
</ul>
<p>This workflow is designed to be relatively easy to master; although some quite advanced tools are used (e.g. R Markdown and git), you only use very limited functionality from each.</p>
</div>
<div id="the-tools" class="section level1">
<h1>The tools</h1>
<p>This workflow uses the following tools:</p>
<ul>
<li><p><strong>Markdown</strong> for writing the manuscript text. Why Markdown? Because it’s much easier than HTML, and therefore, much much much easier than LaTeX. Also, HTML can be included, if desired, as can LaTeX Math expressions, in R Markdown. In this sense Markdown represents the sweet spot between being accessible to new users and powerful enough to fit most use cases.</p></li>
<li><p><strong>R</strong> for the analyses. Why R? Because R is both very powerful and FLOSS. Furthermore, R has a lot of freely available handbooks and tutorials geared towards research in psychology and statistics. Finally, R has R Markdown, which allows embedding chunks of R code in Markdown documents.</p></li>
<li><p><strong>R Markdown</strong> to integrate the analyses with the manuscript text, pretty much for the above reasons.</p></li>
<li><p><strong>RStudio</strong> as an environment to do the writing in. RStudio allows rendering R Markdown documents with the press of a button, and integrates with git.</p></li>
<li><p><strong>git</strong> for version/revision control and syncing. Git is FLOSS and extremely powerful, but also allows relative novices to play around with with relatively few challenges.</p></li>
<li><p><strong>GitLab</strong> as online headquarters and to manage the project. GitLab is a FLOSS git management system with a number of nice extras such as issue management and so-called continuous integration.</p></li>
<li><p><strong>Zotero</strong> for reference management. Zotero is a FLOSS reference manager that has an online API, which is exactly what we need to automagically have updated references in our manuscript.</p></li>
</ul>
</div>
<div id="the-installation" class="section level1">
<h1>The installation</h1>
<p>Of course, before you can collaborate on or start a project with this workflow, you need those tools installed and configured properly. This will only need to happen once on every PC, of course - once everything’s installed and configured, you can use it in all projects.</p>
<p>You need the following:</p>
<ul>
<li><p>An account at a GitLab installation (GitLab over GitHub because GitLab is FLOSS and GitHub isn’t). Either use your institution’s GitLab installation or create a free account at <a href="https://gitlab.com" class="uri">https://gitlab.com</a>.</p></li>
<li><p>An account at a Zotero installation. Again, either use your institution’s installation or create a free account at <a href="https://zotero.org" class="uri">https://zotero.org</a>.</p></li>
<li><p>The following software:</p>
<ul>
<li>R (<a href="https://cran.r-project.org/" class="uri">https://cran.r-project.org/</a>)</li>
<li>The RStudio desktop client (<a href="https://rstudio.com/products/rstudio/#rstudio-desktop" class="uri">https://rstudio.com/products/rstudio/#rstudio-desktop</a>; or access to an RStudio server installation)</li>
<li>git (<a href="https://git-scm.com/" class="uri">https://git-scm.com/</a>)</li>
<li>The Zotero desktop client (<a href="https://www.zotero.org/download/" class="uri">https://www.zotero.org/download/</a>)</li>
</ul></li>
</ul>
<p>Once you installed R, git, and RStudio, RStudio should automatically find both R and git. You can check this by clicking “File” in RStudio, then clicking “New Project”, and then you should be able to create a new project using Version Control. Click it, and then click the Git option. If this works, RStudio found your git. No luck? Reboot your PC. Still no? See <a href="https://happygitwithr.com/rstudio-see-git.html" class="uri">https://happygitwithr.com/rstudio-see-git.html</a>.</p>
</div>
<div id="the-project-set-up" class="section level1">
<h1>The project set-up</h1>
<p>Once you have the necessary software and accounts set up, you only have to do a few things to start a new project.</p>
<ol style="list-style-type: decimal">
<li><p>Log in to GitLab. Create a new project. Try to set a name that is as much as possible self-explanatory (to others, not to you) but not too long, and set the project to Public (it’s <em>Open</em> Science, after all). Once created, you’re taken to the project overview: click the ‘Clone’ button and copy the “https” link.</p></li>
<li><p>Open RStudio. In the File menu, select New Project, and create a new Version Control project, specifically, a Git project. Copy-paste the “Clone URL” that you just copied from GitLab. Pick a good place for the project (mine are all in a “Research” directory) and create it.</p></li>
<li><p>RStudio now created an (as yet mostly empty) directory for you. In that directory, create subdirectories to structure your project.</p></li>
<li><p>Log in to Zotero. Create a new public group (but let only admins add and edit entries). I recommend using the same name you used for the Git project.</p></li>
<li><p>In RStudio, create a new file of the R Script type. Go to … and copy-paste the contents.</p></li>
<li><p>In RStudio, create a new file of the R Markdown type. This will be where you type your manuscript. As a convention, I recommend giving it a filename that’s equal to the git repo name (i.e. you project’s name with only lowercase letters and dashes instead of spaces).</p></li>
<li><p>In the project’s root, create a new file called “.gitlab-ci.yml”, and copy-paste the contents from …</p></li>
</ol>
<p>Now, you have the project set up so that most of the tasks are automated. This means</p>
</div>
<div id="working-in-your-project" class="section level1">
<h1>Working in your project</h1>
<p>Once everything’s set up, you can start working in your project.</p>
<ol style="list-style-type: decimal">
<li></li>
</ol>
</div>
