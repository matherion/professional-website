---
author: GjaltJornPeters
comments: false
date: 2016-12-28 10:06:50+00:00
layout: post
link: https://sciencer.eu/2016/12/fear-is-a-bad-counsellor/
slug: fear-is-a-bad-counsellor
title: Fear is a bad counsellor
wordpress_id: 23
categories:
- Behavior change
---



<p>[ primary audience: behavior change intervention developers ]</p>
<p>Threatening communication is a popular behavior change method used <a href="http://europa.eu/rapid/press-release_MEMO-14-134_en.htm">tobacco packaging</a>, to promote <a href="https://www.youtube.com/watch?v=BRo-2THXaOQ">seatbelt use</a> and <a href="http://www.talkingdrugs.org/5-anti-drugs-campaigns">discourage substance use</a>. However, much research also suggests that it is not the best weapon of choice when the goal is to really change behavior, or even when the goal is to raise awareness or educate people.</p>
<p>How is that paradox possible? This blog post will answer that question.</p>
<!-- more -->
<p>Before going into _why _scaring people is usually a bad idea, it’s useful to think about why it seems the go-to solution for so many intervention developers and advertising agencies.</p>
<div id="the-appeal-of-fear" class="section level2">
<h2>The appeal of fear</h2>
<p>One reason scare tactics seem the go-to- solution for many intervention developers and advertising agencies is the bane of psychology: everybody thinks they know people. And therefore, their own thoughts about what would prevent them from doing something dangerous are probably an indication for what would work for others, as well. And of course, who would engage in risky behavior like smoking, driving without a seatbelt, or using methamphetamine if they were sufficiently aware of the risks? Of course, these risks have already been frequently communicated, but apparently, they never managed to reach the people who are at risk. Apparently, the messages have to be more confronting, more intense. That way, they can break through people’s defenses, confront them with how stupid these behaviors are, and that way, they’d think twice before trying a cigarette or methamphetamine. And indeed, when you ask lay people what would work to deter them from doing something stupid, this is often one of the most popular answers (see Biener, Ji, Gilpin, &amp; Albers, 2004; Goodall &amp; Appiah, 2008; and for an interesting recent study, <a href="http://www.biomedcentral.com/1471-2458/12/1011">Ten Hoor et al., 2012</a>).</p>
<p>However, we’ve known for a while that introspection is flawed: people don’t always have access to their reasons for acting as they do (<a href="http://people.virginia.edu/~tdw/nisbett&amp;wilson.pdf">Nisbett &amp; Wilson, 1977</a>). Especially when dealing with behavior change, things can be more complicated than they seem. And lay people are no behavior change experts - in fact, not even most psychologists are behavior change experts. Asking the target population about their reasons can be very useful; and involving them in the intervention development process is crucial to success - but they are not experts on behavior change, and that responsibility shouldn’t be placed with them.</p>
<p>Besides the intuitive attraction of threatening communication, another reason for its popularity is unavailability of alternatives. That is, the perceived unavailability of alternatives. Not all intervention developers and (fortunately) very few advertising agencies are familiar with intervention development protocols such as <a href="https://en.wikipedia.org/wiki/Intervention_mapping">Intervention Mapping</a>, or the <a href="https://osf.io/ng3xh/">lists of behavior change methods</a> that are available.</p>
<p>So from this perspective, it is understandable that intervention developers frequently employ fear and threats in their health promotion efforts. But why is this a problem?</p>
</div>
<div id="nothing-to-fear-but-fear-itself" class="section level2">
<h2>Nothing to fear but fear itself</h2>
<p>The problem is that humans are not rational. We’re not robots. We were not even designed for most things we do nowadays. One of the things people are generally quite good at is maintaining a positive self-image. And one of the means we employ to this end is distortion of our perceptions.</p>
<p>Of course, we’re always interested in information about risks. Most people quite like living, and prefer to keep on doing so. And if you warn somebody that they shouldn’t, for example, eat chicken breast that is not fully cooked, they will likely comply.</p>
<p>Unfortunately, not all behaviors we warn people about are so easy. Quitting smoking is notoriously hard: in fact, <a href="http://www.cdc.gov/tobacco/data_statistics/fact_sheets/fast_facts/index.htm#use">most smokers want to quit</a>. And when people don’t think they can avoid a threat, there’s a problem to their self-image. It’s not fun engaging in something destructive and being fully, painfully aware of it. So, we have ways of being less aware. Whenever somebody’s not confident they can avert a threat, they will react defensively, for example, citing their grandmother who has been smoking two packs a day for forty years, and is still going strong; or explaining that they work out five times a week or eat half a kilo of broccoli every day.</p>
<p>These dynamics have been <a href="http://onlinelibrary.wiley.com/doi/10.1002/ijop.12042/full">studied for over sixty years</a>. However, the issue is still considered somewhat controversial. To resolve this controversy, my colleagues and me conducted a meta-analysis: a critical assessment of all research in the area (<a href="http://www.tandfonline.com/doi/full/10.1080/17437199.2012.703527#abstract">Peters, Ruiter &amp; Kok, 2013</a>). We first thought long and hard about how it was possible that on the one hand, some studies found that threatening communication <em>worked</em>, while on the other hand, other studies found, and psychological theories predicted, that it would not. We thought of an explanation: earlier review and meta-analyses had failed to be sufficiently rigorous in the selection of the studies. After all, the only studies able to answer the question whether fear can be effective, are those where participants were not only presented with threatening communication, but where their confidence in their ability to eliminate that threat was also enhanced. By employing these more strict inclusion criteria, we found that the theories were right: only when people thought they could avert the threat, it made sense to threaten them. In fact, when people are not confident they can change their behavior to avert a threat, but you threaten them anyway, not only is the message ineffective: it might even backfire.</p>
<p>Clearly, threatening or confronting people is not the way to effective interventions. I hope the message was not too unsettling, because I’ll now tell you that in fact, this doesn’t matter, because:</p>
<p>The real question is not whether threatening people can work.</p>
</div>
<div id="the-real-question-and-its-answer" class="section level2">
<h2>The real question and its answer</h2>
<p>The real question is: what is the most effect method that can be used to promote health? As I said earlier, there exist very, very many. The most extensive list I know is the list of methods included in the Intervention Mapping protocol (<a href="http://www.tandfonline.com/doi/abs/10.1080/17437199.2015.1077155">Kok et al., 2015</a>), which includes 99 methods. Unfortunately, these are not ranked by effectiveness. One of the reasons is that the effectiveness of a method depends on a number of things.</p>
<p>See, with behavior change interventions, you don’t actually target behavior. Instead, people determine their own behavior. All you can do is target what’s in their mind: so-called <em>determinants</em> of behavior. There exist many: a well-known (but relatively irrelevant) determinant is knowledge. Another is attitude: a person’s evaluation of the likelihood and desirability of different consequences of a behavior. Perceived social norms are also quite well-known: do I think my parents approve? Or maybe more importantly, my friends? And not all of these determinants are equally important. For very private behaviors that are never discussed, social norms don’t really matter; and for behaviors that are very rarely performed, habit doesn’t come into the picture, whereas for other behaviors, such as smoking, habit is very important.</p>
<p>And methods for behavior change (or behavior change techniques, BCTs) act on these determinants. So whether any method for behavior change has any chance to change behavior at all, depends first and foremost on whether the determinant it targets actually predicts people’s behavior. Your intervention might very effectively influence people’s knowledge, but if a lack of knowledge wasn’t why they engaged in risky behavior in the first place, it won’t do you much good.</p>
<p>Threatening communications target a determinant called risk perception. For behaviors where this determinant is the most important one, if those behaviors are also very easy to perform, threatening communications might be the most effective method available. However, as I explained, there exist many, many behavior change methods (at least 99, probably hundreds). And these target many, many determinants. The odds of any one method usually being the most effective one, or even among the most effective ones, are negligible. This is true especially for a method so contended as threatening communication.</p>
<p>Instead or resorting to threats, it pays to think about which determinants are the most important ones and which methods will be most effective to change those. Ideally, of course, research it and/or discuss it with experts in your target behavior, target population, and behavior change.</p>
</div>
<div id="practical-recommendations-when-changing-behavior" class="section level2">
<h2>Practical recommendations when changing behavior</h2>
<ul>
<li><p>First establish the causes of the behavior (is the most important determinant risk perception? Social norms? Insufficient skills? Hard-to-break habits?);</p></li>
<li><p>Then identify which methods can change those (see <a href="http://www.tandfonline.com/doi/abs/10.1080/17437199.2015.1077155">this extensive list</a>);</p></li>
<li><p>If you end up choosing threatening communication, make sure of the following two things:</p>
<ul>
<li><p>Either your target population has high self-efficacy (confidence they can perform the desirable behavior), or;</p></li>
<li><p>Your intervention contains an <em>effective</em> component that enhances their self-efficacy to acceptable levels.</p></li>
</ul></li>
</ul>
</div>
<div id="read-more" class="section level2">
<h2>Read more</h2>
<p>There are more pitfalls to successful behavior change, of course. There is no room to explain them all here, but if you’re interested, we try to gently explain them in a number of accessible articles that are freely available at <a href="http://effectivebehaviorchange.com">http://effectivebehaviorchange.com</a>. Also, at <a href="http://fearappeals.com">http://fearappeals.com</a>, a lot of research in threatening communication is collected, like an interview study we did where we explored why threatening communications are so popular (where else did you think I got all that?).</p>
<div class="figure">
<img src="http://practicalhealthpsychology.com/wp-content/uploads/2016/07/effectieve-behavior-change-butterfly.png" alt="effectieve behavior change butterfly" />
<p class="caption">effectieve behavior change butterfly</p>
</div>
</div>
<div id="references" class="section level2">
<h2>References</h2>
<p>Biener, L., Ji, M., Gilpin, E. A., &amp; Albers, A. B. (2004). The impact of emotional tone, message, and broadcast parameters in youth anti-smoking advertisements. <em>Journal of Health Communication</em>, 9(3), 259–274.</p>
<p>Goodall, C., &amp; Appiah, O. (2008). Adolescents’ perceptions of Canadian cigarette package warning labels: Investigating the effects of message framing. <em>Health Communication</em>, 23(2), 117–127.</p>
<p>Kok, G., Gottlieb, N. H., Peters, G. J. Y., Mullen, P. D., Parcel, G. S., Ruiter, R. A., … &amp; Bartholomew, L. K. (2015). A Taxonomy of Behavior Change Methods; an Intervention Mapping Approach. <em>Health psychology review</em>.</p>
<p>Nisbett, R. and T. Wilson (1977). Telling more than we can know: Verbal reports on mental processes. <em>Psychological Review</em> 84(3): 231-259.</p>
<p>Peters, G. J. Y., Ruiter, R. A., &amp; Kok, G. (2013). Threatening communication: a critical re-analysis and a revised meta-analytic test of fear appeal theory. <em>Health Psychology Review</em>, <em>7</em>(sup1), S8-S31.</p>
<p>Ten Hoor, G. A., Peters, G. J. Y., Kalagi, J., de Groot, L., Grootjans, K., Huschens, A., … &amp; Kok, G. (2012). Reactions to threatening health messages. <em>BMC public health</em>, <em>12</em>(1), 1011.</p>
</div>
