---
author: GjaltJornPeters
comments: false
date: 2017-02-01 09:02:28+00:00
layout: post
link: https://sciencer.eu/2017/02/on-the-obsession-with-being-normal/
slug: on-the-obsession-with-being-normal
title: On the obsession with being normal
wordpress_id: 28
categories:
- Foundation of Statistics &amp; Methodology
tags:
- methodology
- normality
- null hypothesis significance testing
---



<p>In statistics, one of the first distributions that one learns about is usually the normal distribution. Not only because it’s pretty, also because it’s ubiquitous.</p>
<p>In addition, the normal distribution is often the reference that is used when discussion other distributions: right skewed is skewed to the right  compared to the normal distribution; when looking at kurtosis, a leptokurtic distribution is relatively spiky compared to the normal distribution: and unimodality is considered the norm, too.</p>
<p>There exist quantitative representations of skewness, kurtosis, and modality (the dip test), and each of these can be tested against a null hypothesis, where the null hypothesis is (almost) always that the skewness, kurtosis, or dip test value of the distribution is equal to that of a normal distribution.</p>
<p>In addition, some statistical tests require that the sampling distribution of the relevant statistic is approximately normal (e.g. the <em>t</em>-test), and some require an even more elusive assumption called multivariate normality.</p>
<p>Perhaps all these bit of knowledge mesh together in people’s minds, or perhaps there’s another explanation: but for some reason, many researchers and almost all students operate on the assumption that their data have to be normally distributed. If they are not, they often resort to, for example, converting their data into categorical variables or transforming the data.</p>
<!-- more -->
<p>However, although scores are often (but not always) normally distributed in the population, they don’t have to be. The distribution of scores observed in a sample is a function of the population distribution and sampling/measurement error, and will therefore often reflect the population distribution: but the sample scores usually don’t need to be normally distributed, either. The <em>t</em>-test, for example, requires a normal _sampling _distribution, an assumption that is met in virtually all situations by virtue of the <a href="https://en.wikipedia.org/wiki/Central_limit_theorem">central limit theorem</a>. The only situations where is isn’t are situations where you’re so underpowered you shouldn’t be conducting a <em>t</em>-test in the first place.</p>
<p>In most situations, lack of normality is first and foremost a good reason to doubt the validity of your design and/or operationalisations. If you expect the population distribution to be normal, a deviant distribution of sample scores is suspicious. Maybe your recruitment procedure selected for certain subpopulations (which decreases the validity of your design); or perhaps your measurement instruments (e.g. questionnaires, reaction time tasks, etc) didn’t work properly (which decreases the validity of those operationalisations). If nothing seems out of the ordinary, depending on the test you want to do, you often won’t have to do anything. As I explained above, for example, the <em>t</em>-test is robust against violations of normality, unless you’re grossly underpowered.</p>
<p>To illustrate this, take the distribution of age in the Netherlands in 2015:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index9-1024x439.png" /></p>
<p>This is quite a non-normal population distribution. Now, let us take samples of different sizes. Lots of them, so that we can see what the sampling distribution of the mean looks like as our sample size increases:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index10-768x1024.png" /></p>
<p>As we see here, already at a sample of 10 participants, the sampling distribution of the mean has obtained approximate normality.</p>
<p>Now, let’s take an extremely skewed distribution: the distribution of the number of times somebody wins something in a lottery:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index11.png" /></p>
<p>This is very skewed to the right. Most people never win anything; some win once, some twice, etc. If we take samples from this distribution, again, the sampling distribution quickly becomes normal:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index12-768x1024.png" /></p>
<p>We need less than 100 participants to get a normal sampling distribution. Note that with 128 participants, you have only 80% power to detect a moderate effect size (Cohen’s <em>d</em> = .5). Most effects in psychology and education science are considerably smaller, and 80% power leaves a probability of Type-2 error (20%) that is much larger than what you accept for the Type 1-error (5%), so if you do a <em>t</em>-test with only 128 participants, you’re being quite underpowered to the degree that it’s questionable whether it’s worth spending resources on the study.</p>
<p>So, in the case of the <em>t</em>-test, normality is no problem. If you’re ever worries, you can use the function normalityAssessment in <a href="http://userfriendlyscience.com/">the userfriendlyscience R package</a> to inspect your sampling distribution (<a href="https://www.rdocumentation.org/packages/userfriendlyscience/versions/0.5-2/topics/normalityAssessment">see this help page</a>).</p>
<p>However, for correlations, the situation is less clear. After all, if one distribution is extremely skewed and the other normal, the maximum attainable correlation can no longer be 1. For example, if almost everybody has the lowest score on one variable (so the distribution is right skewed), but the distribution of the other variable is normal, the all those people with the lowest score will necessarily have different scores on the other variable given the distribution shape. This introduces variation: but not covariance, and so, the possible maximum correlation is lower than 1.</p>
<p>To explore this, I wrote some simulations, where I increased the skewness of one distribution and then looked at maximum correlation that could be obtained. These are the results.</p>
<p>This is what we get when both distributions are normal:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index1.png" /></p>
<p>If we then make the second distribution slightly skewed, this is what we get:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index2.png" /></p>
<p>The maximum obtainable correlation is still quite high - much higher than anything you’d reasonably find in real life. Let’s continue the skewing:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index3.png" /></p>
<p>Ok, now we dipped below the .90. Still, nothing as disastrous as you might intuitively expect based on the high skewness of the second distribution. Let’s push it a bit further:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index4.png" /></p>
<p>Now, if the other distribution is also skewed, in the opposite direction, the effect becomes even stronger:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index5.png" /></p>
<p>And if we exaggerate this, with two extremely skewed distribution (but oppositely skewed), the maximum obtainable correlation drops to .18:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index6.png" /></p>
<p>However, these problems only occur if the skewness is opposite: if both distributions are skewed in the same way, the maximum obtainable correlation isn’t affected:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index7.png" /></p>
<p>If the distributions are skewed in the same direction, they can even be extremely skewed without much danger to the maximum obtainable correlation:</p>
<p><img src="http://sciencer.eu/wp-content/uploads/2017/02/index8.png" /></p>
<p>The picture that emerges (literally, hehe) is that you need very severely skewed distributions before it becomes problematic.</p>
<p>And, even then - if your sample distribution closely matches the population distribution, this still doesn’t have to be problematic. After all, in that case, the correlation may still be an accurate estimator of the correlation in the population: because if in the population, there is almost no variation because the distribution is very skewed, there cannot exist a strong association with another variable, either. Unless, as we just saw, that other variable is similarly skewed. In all these cases, you can still accurately estimate the population correlation using your sample (assuming your sample size is large enough for accurate estimation in the first place, <a href="http://www.tqmp.org/RegularArticles/vol10-2/p124/p124.pdf">see this excellent paper with sample sizes required for estimation of correlation coefficients</a>).</p>
<p>So, in most cases, deviations from normality aren’t problematic. It all boils down to which tests you want to do and what their assumptions are - and to what happens if the assumptions are violated.</p>
<p>So never panic if your scores aren’t normally distributed. Just find somebody who can help you figure out how bad it is exactly (e.g. a statistician). Often, the deviations from normality won’t be a problem.</p>
